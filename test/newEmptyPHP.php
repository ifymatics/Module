<?php
namespace App\PHPfox_Interest\Controller
class indexController extends \phpfox_Component
{
    public function process()
    {
        $template=$this->template();
        $template->setTitle('PHPfox_Interest_page');
        $url=$this->url()->makeUrl('PHPfox_Interest_page');
        $template->setBreadcrumb('PHPfox_Interest_page');
        //add template menu
        $template->buildMenuSection('PHPfox_Interest_page', [
            'profile'=>$this->url()->makeUr('/PHPfox_Interest_page'),
             'skip'=>$this->url()->makeUr('/PHPfox_Interest_page/skip'),
        ]);
    }
}

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

